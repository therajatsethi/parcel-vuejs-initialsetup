# Parcel and VueJS initial setup using NPM

Run using `parcel index.html`

# Approach - 2

You can also run parcel to watch `main.js` and in turn include the compiled version of the `main.js` and `scoped css` into the regular `index.html`

`>>> parcel watch main.js --out-file compiled.js`

The above command will create 2 files in the dist folder.

- compiled.js
- compiled.css

And you can include these into your regular index.html file. (Checkout approach-2 branch of this repo)